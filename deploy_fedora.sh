#!/bin/bash
hostname=$1 #eg. amstrad
management_interface_MAC_ADDRESS=$2 #eg. ac:1f:6b:44:30:c6 (amstrad) usually "System LAN1 MAC address"
IPMI_IP_ADDRESS=$3 #eg. 172.26.2.55 (amstrad)

cobbler system add --name=$hostname --profile=fedora28-x86_64
cobbler system edit --name=$hostname --interface=eno1 --mac=$management_interface_MAC_ADDRESS --hostname=$hostname
cobbler system edit --name=$hostname --power-address=$IPMI_IP_ADDRESS --power-user=ADMIN --power-pass=ADMIN --power-type=ipmilan

./reimage.sh $IPMI_IP_ADDRESS
