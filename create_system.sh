#!/bin/bash

cobbler system add --name=$1 --profile=rhel7.5-x86_64 
cobbler system edit --name=$1 --hostname=$1
cobbler system edit --name=$1 --interface=eno1 --mac=$2
